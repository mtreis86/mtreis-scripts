# mtreis-scripts

Scripts for Common Lisp by Michael Reis



Breakdown of files is as follows:


General scripts:

Scratch is where I test things.
Utils are general utility functions.
Math is mathematical functions.
Arrays are functions for working on arrays.


Tools:

Named-arrays are arrays that can be rotated through built-in methods to access
  without having to either copy the array or loop a bunch to get to the right
  portion.
Cache is a set of functions for caching files including indexed hash tables
  which are hash tables with an index in the form of a ring buffer.


Imported scripts:

From PAIP:
  Debugging provides implementation debugging functions.
  Memo is a couple memoization functions.

From PCL:
  Pathnames allows accessing file paths without extra effort.
  Testing is a set of unit testing functions.
