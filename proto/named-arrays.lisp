(in-package :mtreis-scripts)

;;;; Dimensionally Named Arrays
;;;; Michael Reis <michael@mtreis86.com>

#| The following is a system designed for having arrays that are rotatable
through abstractions rather than being necessary to directly handle them. Use
of this is similar to how row-major-aref and row-major-index work, but with a
parameter for the arrangement of the dimensions.

For instance, in a 2d array with '(row col) as the dimensions,
row-major-aref == named-aref '(col row),
in a 3d with '(row col dep)
row-major would be '(dep col row).

The most signifigant name is last in the order to be equivalent to major order.
|#


(defclass narray ()
  ((array :initarg :array :accessor n-array)
   (names :initarg :names :reader n-names))
  (:documentation "Named arrays have dimensions with names and allow rotation
  of the arrays based on those names."))

(defun make-narray (dimensions names
                         &key (element-type t) initial-element initial-contents
                           adjustable fill-pointer displaced-to
                           displaced-index-offset)
  "Create a new narray."
  (when (check-before-making-narray dimensions names)
    (make-instance 'narray :array
                   (apply #'make-array dimensions :element-type element-type
                          (append
                           (cond
                             (initial-element
                              (list :initial-element initial-element))
                             (initial-contents
                              (list :initial-contents initial-contents)))
                           (list :adjustable adjustable
                                 :fill-pointer fill-pointer
                                 :displaced-to displaced-to
                                 :displaced-index-offset
                                 displaced-index-offset)))
                   :names names)))

(defun before-making-narray-check (dimensions names)
  "Error check called before making narray instances"
  (cond
    ((/= (length dimensions) (length names))
     (error "Names must have the same number of elements as dimensions."))
    ((not (listp names))
     (error "Names must be a list."))
    ((not (equalp names (remove-duplicates-from-list names)))
     (error "Duplicate name found in names.")))
  t)

(defmethod naref ((narray narray) names &rest subscripts)
  "Look up the value at the subscripts location in the named array, rotating
   the array to match the order of names."
  (major-aref narray names (apply #'major-index narray names subscripts)))

(defmethod (setf naref) (args (narray narray) names &rest subscripts)
  "Set the value of the array element given the named-rotation."
  (setf (major-aref narray names
                    (apply #'major-index narray names subscripts))
        args))

(defmethod major-index :before ((narray narray) names &rest subscripts)
  (cond ((/= (length names) (length subscripts) (length (n-names narray)))
         (error "Names and subscripts must equal length of narray."))
        ((null (indexes-below-dimensions subscripts
                                         (narray-dimensions narray names)))
         (error "Subscripts must not be equal to or greater than the dimension
                based on the names."))))

(defun indexes-below-dimensions (indexes dimensions)
  "Return true if all indexes are less than the dimension they are part of."
  (loop for index in indexes
        for dim in dimensions
        when (or (>= index dim)
                 (< index 0))
             do (return-from indexes-below-dimensions nil))
  t)

(defmethod major-index ((narray narray) names &rest subscripts)
  "Return the index of the entry at the subscripts based on rotating
  narray to the names specified."
  (apply #'+ (maplist #'(lambda (x y)
                          (* (first x) (apply #'* (rest y))))
                      subscripts
                      (narray-dimensions narray names))))

(defmethod narray-dimensions ((narray narray) names)
  "Return a copy of array-dimensions sorted by the order of the of names."
  (let ((dimensions (array-dimensions (n-array narray))))
    (sort-list-by-another dimensions (names->indexes narray names))))


(defmethod major-aref :before ((narray narray) names index)
  (when (> index (array-total-size (n-array narray)))
    (error "Index is out of bounds.")))

(defmethod major-naref ((narray narray) names index)
   "Return the entry at the index of the major aref based on rotating the
   narray into the order specified in names. Index is rotated."
  (apply #'aref (n-array narray)
         (sort-list-by-another
          (named-index->subscripts narray names index)
          (names->indexes narray names))))


(defmethod (setf major-naref) (args (narray narray) names index)
  `(setf (apply #'aref (n-array ,narray)
          (sort-list-by-another
           (named-index->subscripts ,narray ,names ,index)
           (names->indexes ,narray ,names)))
         ,args))

(defmethod named-index->subscripts ((narray narray) names index)
   "Return the subscripts location of the index given the rotation specified by
   names."
  ;; (let (subscripts)
  ;;   (loop for name in names
  ;;         with temp-index = index
  ;;         do (let ((dimension (name->index narray name)))
  ;;              (multiple-value-bind (units remainder)
  ;;                  (floor temp-index (array-dimension
  ;;                                     (n-array narray) dimension))
  ;;                (push (if (plusp units) units remainder) subscripts)
  ;;                (setf temp-index remainder))))
  ;;   (nreverse subscripts)))
  (maplist #'print (narray-dimensions narray names)))





(defmethod names->indexes ((narray narray) names)
  "Return a list of indexes that match the positions of the names within
   narray."
  (mapcar #'(lambda (name)
              (name->index narray name))
          names))
 

(defmethod name->index ((narray narray) name)
  "Which dimension of the narray is this name?"
  (position name (n-names narray)))


