(in-package :mtr)

(defvar +day-names+ '("Monday" "Tuesday" "Wednesday" "Thursday" "Friday" "Saturday" "Sunday"))
(defvar *current-time*)

;;; Main time functions that rely on the decoded time, used a closure to ensure stablity

(let (current-hour current-minute current-second current-day-of-week current-month
      current-day current-year current-dst current-zone)
  (defun refresh-time ()
    (setf *current-time* (get-universal-time))
    (decode-time))
  (defun set-time (number)
    (setf *current-time* number)
    (decode-time))
  (defun decode-time (&optional (time *current-time*))
    (multiple-value-bind (second minute hour day month year day-of-week dst-p tz)
        (decode-universal-time time)
      (setf current-hour        hour
            current-minute      minute
            current-second      second
            current-day-of-week (nth day-of-week +day-names+)
            current-month       month
            current-day         day
            current-year        year
            current-dst         dst-p
            current-zone        (- tz)))
    *current-time*)
  (defun get-hour ()
    current-hour)
  (defun get-minute ()
    current-minute)
  (defun get-second ()
    current-second)
  (defun get-day-of-week ()
    current-day-of-week)
  (defun get-month ()
    current-month)
  (defun get-day ()
    current-day)
  (defun get-year ()
    current-year)
  (defun get-dst ()
    current-dst)
  (defun get-zone ()
    current-zone)
  (defun print-time ()
    (format t "~2,'0d:~2,'0d:~2,'0d on ~a, ~d-~d-~2,'0d (GMT ~@d)"
            current-hour current-minute current-second current-day-of-week current-year
            current-month current-day current-zone)
    *current-time*)


;;; Types

 (defun yearp (number)
  (and (integerp number)
       (> number 1800)
       (< number 3000))))

(deftype year () `(satisfies yearp))

(defun u-time-p (number)
  (and (integerp number)
       (>= number 0)
       (< number most-positive-fixnum)))

(deftype u-time () `(satisfies u-time-p))


;;; Date calculation functions

(defun day-of-week (year month day)
  "Take in the year in four digits, month in two digits, and day in two digits and return the day of the week. Cover 1900 01 01 forward."
  (nth (mod (days-since 1900 01 01 year month day) 7)
       +day-names+)) 

(defun days-in-month (year month)
  "Take in the year in four digits, month in two digits, and return the number of days in that month."
  (if (and (= month 02)
           (leap-year-p year))
      29
      (case month
        (02 28)
        ((or 04 06 09 11) 30)
        (otherwise 31))))

(defun days-in-year (year)
  "How many days are in the year?"
  (if (leap-year-p year)
      365
      364))

(defun leap-year-p (year)
  "Take in a four digit year and return true if a leap year."
  (and (zerop (mod year 4))
       (or (zerop (mod year 400))
           (not (zerop (mod year 100))))))

(defun days-since (start-year start-month start-day end-year end-month end-day)
  "How many days from start to end. Same day = 0."
  (let* ((start-gap
           (days-since-new-years start-year start-month start-day))
         (end-gap
           (days-until-new-years end-year end-month end-day))
         (total 0))
    (loop for year from start-year to end-year do
      (loop for month from 01 to 12 do
        (incf total (days-in-month year month))))
    (decf total start-gap)
    (decf total end-gap)
    total))

(defun days-since-new-years (year month day)
  "How many days since new years? 01-01 is 0 days since."
  (let ((total (- day 1)))
    (loop for each-month from 01 below month do
      (incf total (days-in-month year each-month)))
    total))

(defun days-until-new-years (year month day)
  "How many days until new years? 12-31 is 1 day until."
  (let ((total (+ (- (days-in-month year month) day) 1)))
    (loop for each-month from month below 12 do
      (incf total (days-in-month year each-month)))
    total))



