(defpackage :mtreis-scripts
  (:use #:cl)
  (:nicknames #:mtr)
  (:export


   ;;; from PAIP

   ;; from debugging
   #:dbg
   #:start-debugging
   #:stop-debugging
   #:start-profile
   #:stop-profile
   #:profile-report
   #:with-profiling

   ;; from memo
   #:memoize
   #:clear-memoize
   #:defun-memo


   ;;; from PCL

   ;; from testing
   #:deftest
   #:check
   #:combine-results
   #:report-result


   ;;; mtr functions

   ;; from math
   #:fib
   #:primep
   #:prime-sieve
   #:make-prime-vector
   #:palendrome-p
   #:int-length
   #:largest-factor
   #:smallest-factor
   #:list-factors
   #:list-factors-exclusive
   #:exponent-p
   #:root
   #:sum-digits
   #:least
   #:most
   #:factorial
   #:nth-digit
   #:enumerate
   #:print-radix
   #:random-linear
   #:random-float
   #:round-to
   #:random-normal
   #:permutations
   #:reverse-int
   #:euler
   #:tri-height
   #:tri-heron
   #:tri-cosines-angle
   #:tri-cosines
   #:rad->deg
   #:deg->rad
   #:shared-digits
   #:equivalent-fractions
   #:digits-of
   #:multiples-of
   #:sum-vector

   ;; from utils
   #:whitespacep
   #:remove-duplicates-from-list
   #:flatten
   #:append-uniques
   #:tokenize
   #:token
   #:punctuationp
   #:read-csv-to-vector
   #:read-csv-to-list
   #:remove-list
   #:all-but
   #:flatten-to-conses
   #:group-lists
   #:sort-list-by-another
   #:concat-symbols

   ;; from arrays
   #:array-count
   #:array-row
   #:loop-over-2d-array
   #:copy-array
   #:copy-replace-array
   #:array-row
   #:array-col
   #:array-box
   #:array-row-uniques
   #:array-col-uniques
   #:remove-entry-from-array
   #:array-row-contents
   #:array-col-contents
   #:array-box-contents

   ;; from named-arrays
   #:make-narray
   #:naref
   #:major-index
   #:major-naref
   #:narray-dimensions

   ;; from time
   #:refresh-time
   #:set-time
   #:decode-time
   #:get-hour
   #:get-minute
   #:get-second
   #:get-day-of-week
   #:get-month
   #:get-day
   #:get-year
   #:get-dst
   #:print-time
   #:yearp
   #:year
   #:u-time-p
   #:u-time
   #:day-of-week
   #:days-in-month
   #:days-in-year
   #:leap-year-p
   #:days-since
   #:days-since-new-years
   #:days-until-new-years))


