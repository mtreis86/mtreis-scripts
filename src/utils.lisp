(in-package :mtreis-scripts)

(defun whitespacep (char)
  "Return true if the character is a space, tab, newline."
  (if (characterp char)
      (case char
        (#\space t) (#\tab t) (#\newline t) (#\return t))
      (or (string= char #\space)
          (string= char #\tab)
          (string= char #\newline)
          (string= char #\return))))

(defun remove-duplicates-from-list (list)
  "Removes the duplicate entries from the list."
  (loop :with seen := (make-hash-table)
        :with result := '()
        :for entry :in list
        :do (when (null (gethash entry seen))
              (push entry result)
              (push t (gethash entry seen)))
        :finally (return (nreverse result))))

(defun flatten (list)
  "Flatten the list, removing all cons cells, returning the equivalent list."
  (unless (atom list)
    (let ((result nil))
      (labels ((sub-flatten (list)
                 (loop for i in list do
                   (if (consp i)
                       (sub-flatten i)
                       (unless (null i)
                         (push i result))))))
        (sub-flatten list))
      (nreverse result))))

(defun nconc-single-unique (old x)
  "Destructively append x to old if not already in old."
  (unless (member x old)
    (nconc old (list x))))

(defun nconc-uniques (old new)
 "Destructively append new to old if not already in old. Maintain order within lists. Skip duplicates within new."
  (mapcar #'(lambda (x)
              (nconc-single-unique old x))
          new)
  old)

(defun append-uniques (old new)
  "Append new to old if not already in old. Maintain order within lists. Skip duplicates within new."
  (let ((result (copy-list old)))
    (mapcar #'(lambda (x)
                (nconc-single-unique result x))
            new)
    result))
(defun tokenize (string &optional list)
  "Return a list of all the words in the string, ignoring whitespace and punctuation. If list is provided, append the new tokens to the end of list."
  (declare (optimize (debug 3)))
  (multiple-value-bind (token next-index)
      (token string)
    (cond
     ;; Whitespace at the end of the token
     ((or (= next-index 0)
          (= next-index -1))
      list)
     ;; Tokens all collected, no whitespace at the end
     ((>= next-index (length string))
      (append list (list token)))
     ;; More tokens to collect?
     (t (tokenize (subseq string next-index)
             (append list (list token)))))))

(defun test-tokenization ()
  (assert (equal (tokenize ".") '()))
  (assert (equal (tokenize "this. is a test.") '("this" "is" "a" "test")))
  (assert (equal (tokenize "this is a test") '("this" "is" "a" "test")))
  (assert (equal (tokenize "") '()))
  (assert (equal (tokenize "thisisatest") '("thisisatest")))
  (assert (equal (tokenize "t") '("t"))))

(defun token (string &optional first-index (index 0))
  "Return the token formed from the string along with the position the end was found at. If no tokens found, return nil and -1. If the leading characters are whitespace or punctuation, skip them. If handed an empty string, return nil and 0."
  (cond
   ;; Emptry string
   ((= (length string) 0)
    (values nil 0))
   ;;; Single character token
   ((and (= (length string) 1)
         (not (whitespacep (subseq string index (+ index 1))))
         (not (punctuationp (subseq string index (+ index 1)))))
    (values string 1))
   ;; Whitespace/punctuation found at end of token
   ((and (not (null first-index))
         (or (whitespacep (subseq string index (+ index 1)))
             (punctuationp (subseq string index (+ index 1)))))
    (values (subseq string first-index index) (+ index 1)))
   ;; Finished processing, found a token
   ((or (and (not (null first-index))
           (= (length string) (+ index 1))))
    (values (subseq string first-index (+ index 1)) (+ index 1)))
   ;; Finished processing, no tokens found
   ((and (null first-index)
         (= (length string) (+ index 1)))
    (values nil -1))
   ;; Skip whitespace
   ((and (null first-index)
         (or (whitespacep (subseq string index (+ index 1)))
             (punctuationp (subseq string index (+ index 1)))))
    (token string nil (+ index 1)))
   ;; Token starting
   ((null first-index)
    (token string index (+ index 1)))
   ;; Token processing
   (t (token string first-index (+ index 1)))))

(defun punctuationp (char)
  "Return true if the character is punctuation."
  (if (characterp char)
      (case char
        (#\. t) (#\, t) (#\! t) (#\? t) (#\; t) (#\: t))
      (or (string= char #\.) (string= char #\,) (string= char #\!)
          (string= char #\?) (string= char #\;) (string= char #\:))))

(defun read-csv-to-vector (path)
  "Read in the CSV file, using commas as delimiters, returning it as a vector."
  (let ((csv-readtable (copy-readtable))
        (input-vector (make-array 0 :adjustable t :fill-pointer 0)))
    (set-syntax-from-char #\, #\Space csv-readtable)
    (let ((*readtable* csv-readtable))
      (with-open-file (input path)
        (loop for entry = (read input nil nil)
           while entry do
             (vector-push-extend entry input-vector 1))))
    input-vector))

(defun read-csv-to-list (path)
  "Read in the CSV file, using commas as delimiters, returning it as a list."
  (let ((csv-readtable (copy-readtable)))
    (set-syntax-from-char #\, #\Space csv-readtable)
    (let ((*readtable* csv-readtable))
      (with-open-file (input path)
        (loop for entry = (read input nil nil)
              while entry
           collecting entry)))))

(defun remove-list (rem-list list)
  "Remove all entries in rem-list from list and return the result."
  (let (result)
    (loop for entry in list do
      (unless (member entry rem-list)
        (push entry result)))
    (nreverse result)))

(defun all-but (elt list)
  "Return the list minus the entry."
  (remove elt list))

(defun flatten-to-conses (list)
  "Flatten the list, removing all cons cells but the deepest, returning the equivalent list of lists."
  (unless (atom list)
    (let ((result nil))
      (labels ((sub-flatten (list)
                 (loop for entry in list do
                       (if (and (consp entry)
                                (not (atom (first entry)))
                                (or (not (atom (rest entry)))
                                    (null (rest entry))))
                           (sub-flatten entry)
                           (unless (null entry)
                             (push entry result))))))
        (sub-flatten list))
      (nreverse result))))

(defun group-lists (list)
  "Group the entries in the list-of-lists by the first entry, with the second
  in each being a list.
  Eg (group-lists '((a 1) (a 2) (b 3))) => ((a (1 2)) (b (3)))"
  (let (new)
    (loop for entry in list do
      (let* ((key (first entry))
             (val (rest entry))
             (new-entry (assoc key new :test #'equalp)))
        (if (null new-entry)
            (push (cons key (list val)) new)
            (pushnew val (rest new-entry)))))
    new))

(defun concat-symbols (&rest symbols)
  "Combine multiple symbols into one in the typical concatenate fashion."
  (intern (apply #'concatenate 'string (mapcar #'symbol-name symbols))))

(defun sort-list-by-another (sequence key)
  "Sort sequence by key, eg if sequence is '(a b c) and key is '(2 0 1) then
  output will be '(c a b). Sequence and key must be equal length
  and both must be a list."
  (cond ((not (listp sequence))
         (error "List must be a sequence."))
        ((not (listp key))
         (error "Key must be a sequence."))
        ((not (equal (length sequence) (length key)))
         (error "Sequence and key must be equal length."))
        (t (let (result)
             (loop for index in key
                   do (push (elt sequence index) result))
             (nreverse result)))))
