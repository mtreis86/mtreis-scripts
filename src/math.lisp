(in-package :mtreis-scripts)


(defun fib (n)
  "Find the nth fibinacci number."
  (labels ((fibn (current next)
             (when (= 0 n) (RETURN-FROM fibn current))
             (decf n)
             (let ((next (+ current next))
                   (current next))
               (fibn current next))))
    (fibn 1 1)))

(defun-memo primep (n)
  "Is n prime?"
  (declare (optimize (speed 3) (debug 0) (safety 0)))
  (declare (type fixnum n))
  (cond ((= n 2)
         t)
        ((evenp n)
         nil)
        ((<= n 1)
         (error "N must be greater than 1."))
        (t (loop for i from 2 to (floor (sqrt n))
              when (zerop (mod n i)) do (RETURN-FROM primep nil))
           t)))

(defun-memo prime-sieve (high)
  "Return a bit-vector of prime-numbers where the each index of the prime is the number, and each bit represents prime if 1 and not-prime if 0."
  (declare (optimize (speed 3) (debug 0) (safety 0)))
  (declare (type fixnum high))
  (let ((prime-vector (make-array (+ high 1) :adjustable nil :initial-element 1)))
    (labels ((set-non-primes (n)
               (loop for i from n by n
                     until (> i high)
                     when (not (= i n)) do
                       (setf (aref prime-vector i) 0))))
      (set-non-primes 2)
      (loop for i from 3 below high
            when (= (the fixnum (aref prime-vector i)) 1) do
              (set-non-primes i)))
    prime-vector))

(defun make-prime-vector-old (low high)
  "Returns an array of primes between low and high inclusive."
  (let ((primes (make-array 1 :element-type 'integer :adjustable t
                              :fill-pointer 0)))
    (loop for i from low to high do
      (when (primep i)
        (vector-push-extend i primes)))
    primes))

(defun-memo make-prime-vector (low high)
  "Returns an array of primes between low and high inclusive."
  (declare (optimize (speed 3) (debug 0) (safety 0)))
  (declare (type fixnum low high))
  (let ((prime-vector (make-array 1 :element-type 'integer :adjustable t
                               :fill-pointer 0))
        (primes (prime-sieve high)))
    (loop for i fixnum from low to high
          when (= (the bit (svref primes i)) 1) do
            (vector-push-extend i prime-vector))
    prime-vector))

(defun palendrome-p (in)
  "Is n a palendrome?"
  ;;Convert n to a string to prevent zeros from being turned to whitespace, if not already a string
  (declare (optimize debug))
  (let ((in-string (if (stringp in)
                       in
                       (write-to-string in)))
        (output nil))
    (labels ((substep (in-string)
               (if (<= (length in-string) 1)
                   (setf output t)
                   (if (and (= (length in-string) 2)
                            (equal (subseq in-string 0 1)
                                   (subseq in-string 1 2)))
                       (setf output t)
                       (when (equal (subseq in-string 0 1)
                                    (subseq in-string (- (length in-string) 1)))
                         (substep (subseq in-string 1 (- (length in-string) 1))))))))
      (substep in-string))
    output))

(defun int-length (n)
  "How many digits in n?"
  (let ((total 1))
    (labels ((subint (n)
               (when (>= n 10)
                 (progn (incf total)
                        (subint (/ n 10))))))
      (subint (abs n)))
    total))

(defun largest-factor (n)
  "Find the largest factor of n."
  (/ n (smallest-factor n)))

(defun smallest-factor (n)
  "Find the smallest factor of n."
  (let ((result 1))
    (loop for i from 2 to (floor (sqrt n)) do
      (when (= (mod n i) 0) (progn (setf result i)
                                   (RETURN))))
    result))

(defun list-factors (n)
  "Return a list of all the factors of n including itself. Not sorted."
  (flatten (loop for i from 1 to (floor (sqrt n))
                 collecting (when (zerop (mod n i))
                              (list i (/ n i))))))

(defun list-factors-exclusive (n)
  "Return a list of all the factors of n exclusive. Sorted."
  (loop for i from 1 to (floor (/ n 2))
        when (zerop (mod n i))
          collecting i))

(defun exponent-p (number exponent)
  "Does number have roots?"
  ;;(exponent-p 25 2) => t
  ;;(exponent-p 8 3) => t
  (if (>= number 1)
      (zerop (rem (root number exponent) 1))
      (error "Number must be greater than or equal to 1.")))

(defun root (number exponent)
  "Find the exponent root of the number."
  ;;(root 25 2) => 5
  ;;(root 8 3) => 2
  (if (>= number 1)
      (expt number (/ 1 exponent))
      (error "Number must be greater than or equal to 1.")))

(defun sum-digits (n)
  "Return the sum of the digits in the number n. (sum-digits 123) => 6."
  (let ((sum 0))
    (labels ((sum-next (num)
               (if (< num 10)
                   (setf sum (+ sum num))
                   (progn (setf sum (+ sum (mod num 10)))
                          (sum-next (floor num 10))))))
      (sum-next n))
    sum))

(defun least (&rest numbers)
  "Return the smallest of the numbers."
  (loop for i in numbers
        minimizing i))

(defun most (&rest numbers)
  "Return the greatest of the numbers."
  (loop for i in numbers
        maximizing i))

(defun factorial (n)
  "Find the facorial of n. (factorial 4) => 24"
  (let ((product 1))
    (loop for i from n downto 1 do
      (setf product (* product i)))
    product))

(defun nth-digit (number n)
  "Return the nth digit of the number, reversed 0 indexed. (nth-digit 1234 1) => 3."
  (if (= n 0)
      (rem number 10)
      (nth-digit (floor number 10) (- n 1))))

(defun enumerate (list size)
  "Return a 2d array of all possible enumerations of the list with the given size of each sub-array. eg (enumerate '(t f) 3) => #((t t t) (t t f) (t f t) (t f f) (f t t) (f t f) (f f t) (f f f))."
  (declare (optimize (debug 3) (speed 0) (safety 0)))
  (let* ((radix (length list))
         (count (expt radix size))
         (input (make-array radix :initial-contents list))
         (result (make-array (list count size))))
    (loop for i from 0 below count do
      (let ((mask (print-radix i radix size)))
        (loop for j from 0 below size do
          (setf (aref result i j)
                (aref input (aref mask j))))))
    result))

(defun print-radix (number radix size)
  "Return a vector of size with the number printed in the radix. eg (print-radix 3 2 4) => #(0 0 1 1)"
  (let ((raw-num (let ((*print-base* radix))
                   (format nil "~a" number)))
        (result (make-array size)))
    (loop for i from 1 to (length raw-num) do
      (setf (aref result (- (length result) i))
            (digit-char-p (aref raw-num (- (length raw-num) i)))))
    result))

(defun random-linear (low high)
  "Return a random number between low and high inclusive."
  (let ((range (- high low)))
    (+ (random (+ range 1)) low)))

(defun random-float (num-digits)
  "Return a random number between 0 and 1 exclusive with the specified number of digits."
  (if (< num-digits 1)
      (error "Must have a number of digits greater than or equal to one.")
      (let* ((mask (expt 10 num-digits))
             (int-random (random (- mask 1))))
        (float (/ (+ int-random 1) mask)))))

(defun round-to (float &optional (precision 2))
  "Round the float to the number of digits specified."
  (let* ((mask (expt 10 precision))
         (expanded (truncate (* float mask))))
    (float (/ expanded mask))))

(defun random-normal (mu sigma &key (num-digits 4) (precision 4))
  "Return a random number from a normalized gaussian curve with mu as the mean of the curve and sigma as the standard deviation, and num-digits as the length of the float past the decimal."
  (let* ((u1 (random-float precision))
         (u2 (random-float precision))
         (z (* (sqrt (* -2.0 (log u1))) (cos (* 2.0 pi u2)))))
    (round-to (+ (* z sigma) mu) num-digits)))

(defun permutations (list)
  "Return a list of all the permutations of the list without any duplicate entries while maintainging order of input. eg (permutations '(0 1 2)) => ((0 1 2) (0 2 1) (1 0 2) (1 2 0) (2 0 1) (2 1 0))"
  (let (result)
    (labels ((permute (list prev)
               (if (<= (length list) 2)
                   (progn (push (append prev list) result)
                          (push (append prev (list (second list) (first list)))
                                result))
                   (loop for entry in list do
                        (permute (remove-one entry list)
                                 (append prev (list entry))))))
             (remove-one (thing list)
               (let ((pos (position thing list)))
                 (append (subseq list 0 pos)
                         (subseq list (1+ pos))))))
      (permute list nil))
    (nreverse result)))

(defun reverse-int (integer)
  "Return the integer that is the input in reverse eg (reverse-int 23) => 32."
  (multiple-value-bind (int length)
      (parse-integer (reverse (format nil "~A" integer)))
    (declare (ignore length))
    int))

(defun euler (n)
  "Calculate e within n steps."
  (let ((eulersnum 1))
    (loop for step from 1 to n do
         (incf eulersnum (/ 1 (factorial step))))
    eulersnum))

(defun tri-height (area base)
  "Determine the height of a triangle given the area and base."
  (/ (* area 2) base))

(defun tri-heron (a b c)
  "Determine the area of a triangle given the length of the three sides."
  (let ((s (/ (+ a b c) 2)))
    (sqrt (* s (- s a) (- s b) (- s c)))))

(defun tri-cosines-angle (a b c)
  "Determine the angle of a C (the angle opposide the side c) given the three sides."
  (acos (/ (- (+ (expt a 2)
                 (expt b 2))
              (expt c 2))
           (* 2 a b))))

(defun tri-cosines (a b ab)
  "Determine the length of side c given the sides a and b, and the angle between them."
  (sqrt (- (+ (expt a 2)
              (expt b 2))
           (* 2 a b (cos ab)))))

(defun rad->deg (radians)
  "Convert radians to degrees."
  (/ (* radians 180) pi))

(defun deg->rad (degrees)
  "Convert degrees to radians."
  (/ (* degrees pi) 180))

(defun shared-digits (&rest nums)
  "Return digits if nums share any digits, otherwise nil."
  (let (digits shared)
    (loop for num in nums do
         (let ((printed (format nil "~A" num)))
           (loop for index from 0 below (length printed) do
                (let ((digit (parse-integer (subseq printed index (+ index 1)))))
                  (if (member digit digits)
                      (setf shared (adjoin digit shared))
                      (push digit digits))))))
    (if shared
        shared
        nil)))

(defun equivalent-fractions (fraction length)
  "Return a list of equivalent fractions with the numerator and denominator both being below the length. Return will be in string form as fractions would otherwise be reduced."
  (let (list)
    (loop for mult from 1
       until (or (> (int-length (* (numerator fraction) mult)) length)
                 (> (int-length (* (denominator fraction) mult)) length)) do
         (push (format nil "~A/~A" (* (numerator fraction) mult)
                       (* (denominator fraction) mult))
               list))
    (nreverse list)))

(defun digits-of-hacky (number)
  "Return a list of the digits in the number. Keeps them in order."
  (let ((printed (format nil "~A" number))
        (digits nil))
    (loop for index from 0 below (length printed) do
         (let ((digit (parse-integer (subseq printed index (+ index 1)))))
           (push digit digits)))
    (nreverse digits)))
    
(defun digits-of (num &optional (acc nil))
  (if (zerop num)
      acc
      (multiple-value-bind (div rem)
          (floor (abs num) 10)
        (digits-of div (append (list rem) acc)))))

(defun multiples-of (number max)
  "What are the multiples of number beneath max?"
  (loop for n from 2
       until (> (* n number) max)
       collecting (* n number)))

(defun sum-vector (vector)
  "What is the sum of the entries in the vector?"
  (loop for entry across vector summing entry))









