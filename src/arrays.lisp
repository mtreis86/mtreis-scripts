(in-package :mtreis-scripts)


(defun array-count (array row)
  "Count the unique entries in the row of the array, returning the entries and their counts in two lists."
  (let ((entries nil)
        (counts nil))
    (loop for i across (if (vectorp array)
                           array
                           (array-row array row)) do
                             (if (member i entries :test 'equal)
                                 (incf (elt counts (position i entries :test 'equal)))
                                 (progn (push i entries)
                                        (push 1 counts))))
    (values (list entries counts))))

(defun array-row (array row)
  "Return the row of the array."
  (let ((size (array-dimension array 1)))
    (make-array size :displaced-to array
                     :displaced-index-offset (* row size))))

(defmacro loop-over-2d-array (array &body body)
  "Loop for each entry in the array, made accessable by (aref array row col)."
  `(loop for row from 0 below (array-dimension ,array 0) do
    (loop for col from 0 below (array-dimension ,array 1)
          ,@body)))

(defun copy-array (array)
  "Copy an array including key args."
  (let ((new-array (make-array (array-dimensions array)
                               :element-type (array-element-type array)
                               :adjustable (adjustable-array-p array)
                               :fill-pointer
                               (when (array-has-fill-pointer-p array)
                                 (fill-pointer array)))))
    (dotimes (index (array-total-size array))
      (setf (row-major-aref new-array index)
            (row-major-aref array index)))
    new-array))

(defun copy-replace-array (array match-map)
  "Build a new array where the objects in the array are replaced based on the match map, an alist containing (old . new) entries. Note, this will not affect nested arrays or vectors."
  (let ((new-array (make-array (array-dimensions array)
                               :element-type (array-element-type array)
                               :adjustable (adjustable-array-p array)
                               :fill-pointer
                               (when (array-has-fill-pointer-p array)
                                 (fill-pointer array)))))
    (dotimes (index (array-total-size array))
      (let* ((entry (row-major-aref array index))
             (match (assoc entry match-map)))
        (setf (row-major-aref new-array index)
              (if match
                  (rest match)
                  entry))))
    new-array))


(defun array-row (array row)
  "Take the array and return the row as a new array."
  (let* ((size (array-dimension array 1))
         (new (make-array size)))
    (loop for col from 0 below size do
      (setf (aref new col)
            (aref array row col)))
    new))

(defun array-col (array col)
  "Take the array and return the column as a new array."
  (let* ((size (array-dimension array 0))
         (new (make-array size)))
    (loop for row from 0 below size do
      (setf (aref new row)
            (aref array row col)))
    new))

(defun array-box (array row-min row-max col-min col-max)
  "Take the 2d array and return a new array with the entries from in the box
  specified by the range."
  (let* ((rows (- row-max row-min))
         (cols (- col-max col-min))
         (new (make-array (list rows cols))))
    (loop for row from row-min to row-max
          for new-row from 0 below rows do
            (loop for col from col-min to col-max
                  for new-col from 0 below cols do
                    (setf (aref new new-row new-col)
                          (aref array row col))))))


(defun array-row-uniques (array row row-min row-max col-min col-max)
  "What entries in the row of the array-of-lists are unique to that row
  within the row/col range?"
  (let ((contents (array-row-contents array row col-min col-max))
        (removals nil)
        (result))
    (loop for rrow from row-min to row-max
          unless (= rrow row) do
            (setf removals (union removals (array-row-contents
                                            array rrow col-min col-max))))
    (loop for entry in contents
          when (not (member entry removals))
            do (push entry result))
    (sort result #'<)))

(defun array-col-uniques (array col row-min row-max col-min col-max)
  "What entries in the column of the array-of-lists are unique to that
  column within the row/col range?"
  (let ((contents (array-col-contents array col row-min row-max))
        (removals nil)
        (result))
    (loop for rcol from col-min to col-max
          unless (= rcol col) do
            (setf removals (union removals (array-col-contents
                                            array rcol row-min row-max))))
    (loop for entry in contents
          when (not (member entry removals))
            do (push entry result))
    (sort result #'<)))

(defun remove-entry-from-array (array entry row-min row-max col-min col-max)
  "Remove the entry from the array where found between the row/col range"
  (loop for row from row-min to row-max do
    (loop for col from col-min to col-max do
      (let ((entry-poss (aref array row col)))
        (when (member entry entry-poss)
          (setf (aref array row col) (all-but entry entry-poss))))))
  array)

(defun array-row-contents (array row col-min col-max)
  "What are the contents of the array in the row between the columns?"
  (array-box-contents array row row col-min col-max))

(defun array-col-contents (array col row-min row-max)
  "What are the contents of the array in the column between the rows?"
  (array-box-contents array row-min row-max col col))

(defun array-box-contents (array row-min row-max col-min col-max)
  "What are the contents of the array within the range?"
  (let (result)
    (loop for row from row-min to row-max do
      (loop for col from col-min to col-max do
        (loop for entry in (aref array row col) do
          (when (not (member entry result))
            (push entry result)))))
    (sort result #'<)))
