(in-package :mtreis-scripts-tests)

(deftest test-make-narray ()
  (let ((test-constructor (make-narray '(2 3 4) '(x y z)))
        (test-direct (make-instance 'mtr::narray
                                    :array (make-array '(2 3 4))
                                    :names '(x y z))))
    (check
      (equalp (mtr::n-array test-constructor)
              (mtr::n-array test-direct))
      (eql (mtr::n-names test-constructor)
           (mtr::n-names test-direct)))))

(deftest test-before-making-narray-check ()
  (check
    (mtr::before-making-narray-check '(2 3 4) '(x y z))
    ;; Wrong size
    (null (ignore-errors (mtr::before-making-narray-check '(1 2) '(x y z))))
    ;; Names not a list
    (null (ignore-errors (mtr::before-making-narray-check '(1 2) 'a)))
    ;; Duplicate names found
    (null (ignore-errors (mtr::before-making-narray-check '(1 2) '(a a))))))

;; not working yet
(deftest test-naref ()
  (let ((test (make-narray '(2 2) '(x y))))
    (setf (aref (mtr::n-array test) 0 1) "zero")
    (check
      (eql (naref test '(x y) 0 1) "zero")
      (eql (naref test '(y x) 1 0) "zero"))
    test))

;; not working yet
(deftest test-setf-naref ()
  (let ((test (make-narray '(2 2) '(x y))))
    (setf (naref test '(x y) 0 1) "zero")
    (check (eql (aref (mtr::n-array test) 0 1) "zero"))
    test))

(deftest test-major-index ()
  (let ((test (make-narray '(2 3 4) '(x y z))))
    (check
      (= (major-index test '(x y z) 1 2 3) 23))
    test))

(deftest test-narray-dimensions ()
  (let ((test (make-narray '(2 3 4) '(x y z))))
    (check
      (equal (narray-dimensions test '(x y z)) '(2 3 4))
      (equal (narray-dimensions test '(x z y)) '(2 4 3))
      (equal (narray-dimensions test '(z y x)) '(4 3 2)))))



















