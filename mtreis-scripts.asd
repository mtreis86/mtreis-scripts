#|
  This file is a part of mtreis-scripts project.
|#

(defsystem #:mtreis-scripts
  :version "0.0.0"
  :author "Michael Reis"
  :license "AGPL3.0"
  :depends-on ()
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "utils")
                 (:file "math" :depends-on ("memo"))
                 (:file "arrays")
                 (:file "time")
                 ;; From PAIP - See license LICENSE-PAIP
                 (:file "memo")
                 (:file "debugging")
                 ;; From PCL - See license LICENSE-PCL
                 (:file "testing"))))
  :description "Various scripts for Common Lisp"
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "mtreis-scripts-tests"))))
