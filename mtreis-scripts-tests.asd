#|
  This file is a part of mtreis-scripts project.
|#

(defsystem #:mtreis-scripts-tests
  :author "Michael Reis"
  :license "AGPL3.0"
  :depends-on (#:mtreis-scripts)
  :components ((:module "tests"
                :components
                ((:file "packages")
                 (:file "named-arrays"))))
  :description "Test system for mtreis-scripts")

